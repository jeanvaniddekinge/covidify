import csv
import pandas as pd
from datetime import datetime as dt
import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
import numpy as np
import time
from shapely.geometry import Polygon
import sys
import pyodbc

pd.set_option("display.precision", 10)

##################################################################
# TRACK EXPOSURE
# This method takes all the infected user data on the server and
# then checks if an indovidual users dataset has crossed paths
# with any of the infected users over the last Avg 7 days
#
# TO DO: Add error logging
##################################################################

# GLOBAL VARIABLES
#INPUT_USERID = '-1301707647'
DAYS_HISTORY = 7  # Average is 5 days
# https://www.healthline.com/health/coronavirus-incubation-period#incubation-period
DIAMOND_SIZE_DEGREES = 0.000278
# 0.000278 = 1 second = lattitude 101 feet, longitude 80 feet
OUTPUT_EXPOSURE = pd.DataFrame(columns=[
                               'overlap', 'Date', 'userID', 'longitudeMin', 'longitudeMax', 'latitudeMin', 'latitudeMax'])

# FUNCTIONS
def calculate_iou(box_1, box_2):
    poly_1 = Polygon(box_1)
    poly_2 = Polygon(box_2)
    iou = poly_1.intersection(poly_2).area / poly_1.union(poly_2).area
    return iou

# compile patient data 
patientData = pd.DataFrame(columns=[
                           'Date', 'userID', 'longitudeMin', 'longitudeMax', 'latitudeMin', 'latitudeMax'])
patientData_detail = pd.DataFrame(
    columns=['Date', 'userID', 'covidStatus', 'latitude', 'longitude', 'timestamp'])

conn = pyodbc.connect('DRIVER={SQL Server};SERVER=102.130.113.152;PORT=1433;DATABASE=Covidify;Trusted_Connection=yes;')
# Query into dataframe
patientData = pd.io.sql.read_sql('SELECT [Date], userID, MIN(longitude) AS longitudeMin, MAX(longitude) AS longitudeMax, MIN(latitude) AS latitudeMin, MAX(latitude) AS latitudeMax FROM [Covidify].[dbo].[PatientData] WHERE [Date] >= CAST(DATEADD(day,-8,GETDATE()) AS DATE) GROUP BY [Date], userID;', conn)
patientData_detail = pd.io.sql.read_sql('SELECT [Date], [userID], [covidStatus], [latitude], [longitude], [timestamp] FROM [Covidify].[dbo].[PatientData] WHERE [Date] >= CAST(DATEADD(day,-8,GETDATE()) AS DATE); ', conn)

patientData_detail["latitude"] = patientData_detail.latitude.astype(float)
patientData_detail["longitude"] = patientData_detail.longitude.astype(float)

# BUILD DETAIL DIAMONDS
# In this algorithm we build a diamond around each gps point as a collision box
patientData_detail["a"] = patientData_detail["latitude"]-DIAMOND_SIZE_DEGREES
patientData_detail["b"] = patientData_detail["latitude"]+DIAMOND_SIZE_DEGREES
patientData_detail["c"] = patientData_detail["longitude"]-DIAMOND_SIZE_DEGREES
patientData_detail["d"] = patientData_detail["longitude"]+DIAMOND_SIZE_DEGREES

print(patientData)
print(patientData_detail)
# pickle patient data
patientData.to_pickle("patientData.pkl")
patientData_detail.to_pickle("patientData_detail.pkl")
