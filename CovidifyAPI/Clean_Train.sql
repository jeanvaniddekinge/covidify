USE DS;
GO

/*
import the data from here into table dbo.latestdata
https://github.com/beoutbreakprepared/nCoV2019/tree/master/latest_data
*/
IF OBJECT_ID('tempdb..#countries') IS NOT NULL
    DROP TABLE #countries;
CREATE TABLE #countries
(country    NVARCHAR(256) NOT NULL PRIMARY KEY, 
 case_Count INT, 
 Negative   INT, 
 Positive   INT
);
IF OBJECT_ID('tempdb..#data') IS NOT NULL
    DROP TABLE #data;
CREATE TABLE #data
(age                     NVARCHAR(32), 
 sex                     NVARCHAR(32), 
 country                 NVARCHAR(128), 
 chronic_disease_binary  BIT, 
 chronic_disease         NVARCHAR(1086), 
 outcome                 NVARCHAR(1086), 
 date_confirmation       DATE, 
 date_death_or_discharge NVARCHAR(128), 
 AGE_                    INT, 
 SEX_                    INT, 
 CHRONIC_                INT, 
 CHRONICCOUNT_           INT, 
 OUTCOME_                INT, 
 HEART_                  INT, 
 CANCER_                 INT, 
 LIVER_                  INT, 
 KIDNEY_                 INT, 
 DIABETES_               INT, 
 LUNGS_                  INT, 
 PROSTATE_               INT,
 HYPERTENSION_            INT,
 OTHER_     INT
);
INSERT INTO #countries
(country, 
 case_Count, 
 Negative, 
 Positive
)
       SELECT country, 
              COUNT(1) case_Count, 
              SUM(CASE
                      WHEN chronic_disease_binary = 'FALSE'
                      THEN 1
                      ELSE 0
                  END) AS Negative, 
              SUM(CASE
                      WHEN chronic_disease_binary = 'TRUE'
                      THEN 1
                      ELSE 0
                  END) AS Positive
       FROM [DS].[dbo].[latestdata]
       WHERE outcome IS NOT NULL
             AND country IS NOT NULL
       GROUP BY country
       HAVING SUM(CASE
                      WHEN chronic_disease_binary = 'TRUE'
                      THEN 1
                      ELSE 0
                  END) <> 0;
INSERT INTO #data
(age, 
 sex, 
 country, 
 chronic_disease_binary, 
 chronic_disease, 
 outcome, 
 date_confirmation, 
 date_death_or_discharge, 
 HEART_, 
 CANCER_, 
 LIVER_, 
 KIDNEY_, 
 DIABETES_, 
 LUNGS_, 
 PROSTATE_,
 HYPERTENSION_,
 OTHER_
)
       SELECT ISNULL(age, -1) age, 
              ISNULL(sex, 'unknown') sex, 
              c.country, 
              ISNULL(chronic_disease_binary, 'unknown') chronic_disease_binary,
              CASE
                  WHEN ISNULL(chronic_disease_binary, 'unknown') = 'True'
                       AND chronic_disease IS NULL
                  THEN 'unknown'
                  ELSE ISNULL(chronic_disease, 'none')
              END chronic_disease, 
              ISNULL(outcome, 'unknown') outcome, 
              RIGHT(date_confirmation, 4) + '-' + SUBSTRING(date_confirmation, 4, 2) + '-' + RIGHT(date_confirmation, 2) date_confirmation, 
              ISNULL(RIGHT(date_death_or_discharge, 4) + '-' + SUBSTRING(date_death_or_discharge, 4, 2) + '-' + RIGHT(date_death_or_discharge, 2), 'unknown') date_death_or_discharge, 
              0 AS HEART_, 
              0 AS CANCER_, 
              0 AS LIVER_, 
              0 AS KIDNEY_, 
              0 AS DIABETES_, 
              0 AS LUNGS_, 
              0 AS PROSTATE_,
			  0 AS HYPERTENSION_,
			  0 AS OTHER_
       FROM [DS].[dbo].[latestdata] ld
            JOIN #countries c ON c.Country = ld.country
       WHERE outcome IS NOT NULL;
UPDATE d
  SET 
      age = (CAST(RIGHT(age, 2) AS INT) + CAST(LEFT(age, 2) AS INT)) / 2
FROM #data d
WHERE age <> '-1'
      AND CHARINDEX('-', age) <> 0
      AND (ISNUMERIC(RIGHT(age, 2)) = 1
           AND ISNUMERIC(LEFT(age, 2)) = 1);
UPDATE #data
  SET 
      age = REPLACE(age, '-', '')
WHERE age <> '-1'
      AND CHARINDEX('-', age) <> 0
      AND (ISNUMERIC(RIGHT(age, 2)) <> 1
           OR ISNUMERIC(LEFT(age, 2)) <> 1);
UPDATE #data
  SET 
      HEART_ = 1
WHERE chronic_disease LIKE '%HEART_%'
      OR chronic_disease LIKE '%coronary%'
      OR chronic_disease LIKE '%infarction%'
      OR chronic_disease LIKE '%pulmonary%'
      OR chronic_disease LIKE '%cardiac%';
UPDATE #data
  SET 
      CANCER_ = 1
WHERE chronic_disease LIKE '%CANCER_%';
UPDATE #data
  SET 
      LIVER_ = 1
WHERE chronic_disease LIKE '%hepatitis%'
      OR chronic_disease LIKE '%thyroid%';
UPDATE #data
  SET 
      KIDNEY_ = 1
WHERE chronic_disease LIKE '%KIDNEY_%';
UPDATE #data
  SET 
      DIABETES_ = 1
WHERE chronic_disease LIKE '%diabetes%'

UPDATE #data
  SET 
      LUNGS_ = 1
WHERE chronic_disease LIKE '%bronchitis%'
      OR chronic_disease LIKE '%asthma%'
      OR chronic_disease LIKE '%COPD%';
UPDATE #data
  SET 
      PROSTATE_ = 1
WHERE chronic_disease LIKE '%PROSTATE_%';
UPDATE #data
  SET 
      HYPERTENSION_ = 1
WHERE chronic_disease LIKE '%hyperten%'

UPDATE #data
  SET 
      OTHER_ = 1
WHERE CAST(HEART_ AS INT) + CAST(CANCER_ AS INT) + CAST(LIVER_ AS INT) + CAST(KIDNEY_ AS INT) + CAST(DIABETES_ AS INT) + CAST(LUNGS_ AS INT) + CAST(PROSTATE_ AS INT)+ CAST(HYPERTENSION_ AS INT) = 0
      AND chronic_disease NOT IN('none', 'unknown');
UPDATE #data
  SET 
      OUTCOME_ = 1
WHERE outcome LIKE '%recover%'
      OR outcome LIKE '%stable%'
      OR outcome LIKE '%discharge%'
      OR outcome LIKE '%release%'
      OR outcome LIKE '%alive%'
      OR (outcome LIKE '%treat%'
          AND date_confirmation < DATEADD(day, -42, GETDATE())); -- 604

UPDATE #data
  SET 
      OUTCOME_ = 0
WHERE NOT(outcome LIKE '%recover%'
          OR outcome LIKE '%stable%'
          OR outcome LIKE '%discharge%'
          OR outcome LIKE '%release%'
          OR outcome LIKE '%alive%'
          OR (outcome LIKE '%treat%'
              AND date_confirmation < DATEADD(day, -42, GETDATE()))); -- 423

UPDATE #data
  SET 
      AGE_ = ROUND(age, 0), 
      SEX_ = CASE
                 WHEN sex = 'female'
                 THEN 1
                 WHEN sex = 'male'
                 THEN 0
                 ELSE-1
             END, 
      CHRONIC_ = chronic_disease_binary;
UPDATE #data
  SET 
      CHRONICCOUNT_ = HEART_ + CANCER_ + LIVER_ + KIDNEY_ + DIABETES_ + LUNGS_ + PROSTATE_ + OTHER_;
SELECT *
FROM #data
ORDER BY CHRONICCOUNT_;