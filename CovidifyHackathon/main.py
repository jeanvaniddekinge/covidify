import pandas as pd
from shapely.geometry import Polygon
import datetime
import time
import numpy as np
import json
pd.set_option('display.max_rows', 50)

##################################################################
# THIS ALGORTIHM TAKES THE USER GPS DATA STORED IN userData.json
# AND THEN COMPARES IT TO THE INFECTED USERS GPS DATA STORED IN 
# patientData.pkl and patientData_detail.pkl TO DETERMINE IF THE 
# USER CAME INTO CLOSE CONTACT WITH AN INFECTED USER IN A CERTAIN 
# TIME SPAN
##################################################################

# GLOBAL VARIABLES

DIAMOND_SIZE_DEGREES = 0.000278 # 0.000278 = 1 second = lattitude 101 feet, longitude 80 feet
EXPOSURE_PERIOD_MINUTES = 30
OVERLAP_THRESHOLD_BOX = 0.0   
OVERLAP_THRESHOLD_DIAMOND = 0.5 
debug = True

# FUNCTIONS

# check for overlap of two boxes
def calculate_iou(box_1, box_2):
    poly_1 = Polygon(box_1)
    poly_2 = Polygon(box_2)
    try:
        iou = poly_1.intersection(poly_2).area / poly_1.union(poly_2).area
    except:
        iou = 0
    return iou

def convertMillisToDateTime(millis):
    s = millis / 1000.0
    tempDateTime = datetime.datetime.fromtimestamp(
        s).strftime('%Y-%m-%d %H:%M:%S.%f')
    dateTimeReturn = datetime.datetime.strptime(
        tempDateTime, "%Y-%m-%d %H:%M:%S.%f")
    return dateTimeReturn

# this function checks if the user has come into contact with an infected patient
def track():
    start_time = time.time() # for performance debugging
    # get the users data 
    with open('userData.json') as json_file:
        data = json.load(json_file)
    userData_detail = pd.json_normalize(data)
    print(userData_detail)
    OUTPUT_EXPOSURE = pd.DataFrame(columns=[
                                   'overlap', 'Date', 'userID', 'longitudeMin', 'longitudeMax', 'latitudeMin', 'latitudeMax'])
    OUTPUT_EXPOSURE_DETAIL = pd.DataFrame(
        columns=['overlap', 'Date', 'userID', 'latitude', 'longitude', 'a', 'b', 'c', 'd', 'timestamp', 'covidStatus'])
    # get the patients data
    patientData = pd.read_pickle("patientData.pkl")
    patientData_detail = pd.read_pickle("patientData_detail.pkl")
    userData = pd.DataFrame(columns=[
                                    'Date', 'userID', 'longitudeMin', 'longitudeMax', 'latitudeMin', 'latitudeMax'])
        
    # create a dataframe of the users detail data to match the patient data
    userData_detail['userID'] = userData_detail['userID'].astype(str)
    userData_detail['timestamp'] = userData_detail['timestamp'].astype(float)
    userData_detail['latitude'] = userData_detail['latitude'].astype(float)
    userData_detail['longitude'] = userData_detail['longitude'].astype(float)
    userData_detail['Date'] = userData_detail['Date'].astype(str)

    userData_detail["a"] = userData_detail["latitude"]-DIAMOND_SIZE_DEGREES
    userData_detail["b"] = userData_detail["latitude"]+DIAMOND_SIZE_DEGREES
    userData_detail["c"] = userData_detail["longitude"]-DIAMOND_SIZE_DEGREES
    userData_detail["d"] = userData_detail["longitude"]+DIAMOND_SIZE_DEGREES

    # create a dataframe of the users aggregated data to match the patient data
    UserID = np.array(
            userData_detail.userID.unique())
    INPUT_USERID = str(UserID[0])
    historicDays = np.array(
            userData_detail.Date.unique())
    
    # loop through to get the bounding box for each day
    for dateString in historicDays:
        userTemp = userData_detail[userData_detail["Date"] == dateString]
        patientRow = np.array([dateString, INPUT_USERID, userTemp.longitude.min(), userTemp.longitude.max(), userTemp.latitude.min(), userTemp.latitude.max()])

        tempData = pd.DataFrame(columns=[
                                    'Date', 'userID', 'longitudeMin', 'longitudeMax', 'latitudeMin', 'latitudeMax'])
        tempData.loc[0] = patientRow
        userData = userData.append(tempData, ignore_index=True)


    # always make the data types what you want upfront (else enjoy a world of pain)
    patientData['userID'] = patientData['userID'].astype(str)
    patientData_detail['userID'] = patientData_detail['userID'].astype(str)
    patientData['Date'] = patientData['Date'].astype(str)
    patientData_detail['Date'] = patientData_detail['Date'].astype(str)
    patientData_detail['timestamp'] = patientData_detail['timestamp'].astype(
        float)

    patientData = patientData.drop_duplicates()
    patientData_detail = patientData_detail.drop_duplicates()

    patientData = patientData.loc[patientData['userID'] != INPUT_USERID]
    patientDetail = patientData_detail.loc[patientData_detail['userID']
                                           != INPUT_USERID]

    print(userData)
    print(patientData)
    print('built user data in')
    print("--- %s seconds ---" % (time.time() - start_time))
    start_time = time.time()

    ##################################################################
    # ITERATE THROUGH EACH PATIENT BY DAY COMPARING TO USER
    # IF PATIENT GEO X USER GEO DISTANCE <= 100 METERS
    # ADD RECORD TO RESULTS
    ##################################################################

    for i1, r1 in userData.iterrows():
        for i2,  r2 in patientData.iterrows():
            # only check if paths overlap on the same day
            if r1['Date'] == r2['Date']:
                box_1 = [[float(r1['longitudeMin']), float(r1['latitudeMin'])], [float(r1['longitudeMax']), float(r1['latitudeMin'])], [
                    float(r1['longitudeMax']), float(r1['latitudeMax'])], [float(r1['longitudeMin']), float(r1['latitudeMax'])]]
                box_2 = [[float(r2['longitudeMin']), float(r2['latitudeMin'])], [float(r2['longitudeMax']), float(r2['latitudeMin'])], [
                    float(r2['longitudeMax']), float(r2['latitudeMax'])], [float(r2['longitudeMin']), float(r2['latitudeMax'])]]     
                path_overlap = calculate_iou(box_1, box_2)
                if path_overlap > OVERLAP_THRESHOLD_BOX:
                    r2['overlap'] = path_overlap
                    OUTPUT_EXPOSURE = OUTPUT_EXPOSURE.append(
                        r2, ignore_index=True)
     
    print(OUTPUT_EXPOSURE)
    print('built high level overlaps in ')
    print("--- %s seconds ---" % (time.time() - start_time))
    start_time = time.time()
    # PROCESS DETAIL LEVEL
    # POPULATE DETAIL DIAMONDS
    for i1, r1 in OUTPUT_EXPOSURE.iterrows():
        # get the diamonds for this patient and user on this date
        tempUserDiamonds = userData_detail.loc[userData_detail['Date'] == r1['Date']]
        # loop through each patient record and check for overlaps wwith each user reord (ouch)
        for i2, r2 in tempUserDiamonds.iterrows():        
            # get user bounding box
            # filter patient to bounding box of user
            tempPatientDiamonds = patientDetail.loc[(
                patientDetail['userID'] == r1['userID']) & (patientDetail['Date'] == r1['Date'])
                & (patientDetail['longitude'] >= r2.c)
                & (patientDetail['longitude'] <= r2.d)
                & (patientDetail['latitude'] >= r2.a)
                & (patientDetail['latitude'] <= r2.b)]
            for i3, r3 in tempPatientDiamonds.iterrows():
                # make sure the patient was close in the same 1 hour period as the patient
                # get 30 minutes in milliseconds
                if (r2['timestamp'] < r3['timestamp'] + EXPOSURE_PERIOD_MINUTES*1000*60) & (r2['timestamp'] > r3['timestamp'] - EXPOSURE_PERIOD_MINUTES*1000*60):
                    box_1 = [[float(r3['c']), float(r3['a'])], [float(r3['d']), float(r3['a'])], [
                        float(r3['d']), float(r3['b'])], [float(r3['c']), float(r3['b'])]]
                    box_2 = [[float(r2['c']), float(r2['a'])], [float(r2['d']), float(r2['a'])], [
                        float(r2['d']), float(r2['b'])], [float(r2['c']), float(r2['b'])]]
                    path_overlap = calculate_iou(box_1, box_2)
                    if path_overlap > OVERLAP_THRESHOLD_DIAMOND:
                        r3['overlap'] = path_overlap
                        OUTPUT_EXPOSURE_DETAIL = OUTPUT_EXPOSURE_DETAIL.append(
                            r3, ignore_index=True)

    OUTPUT_EXPOSURE_DETAIL = OUTPUT_EXPOSURE_DETAIL.drop(
        ['a', 'b', 'c', 'd', 'covidStatus', 'latitude', 'longitude'], 1)
    OUTPUT_EXPOSURE_DETAIL = OUTPUT_EXPOSURE_DETAIL.drop_duplicates()

    # last step extract highest overlap per user day
    OUTPUT_EXPOSURE_SUMMARY = OUTPUT_EXPOSURE_DETAIL.loc[OUTPUT_EXPOSURE_DETAIL.groupby(
        ['userID', 'Date'])['overlap'].idxmax()]

    OUTPUT_EXPOSURE_SUMMARY['Time'] = OUTPUT_EXPOSURE_SUMMARY['timestamp'].apply(
        convertMillisToDateTime)
    OUTPUT_EXPOSURE_SUMMARY['Time'] = OUTPUT_EXPOSURE_SUMMARY['Time'].apply(
        lambda x: x.strftime('%H:%M:%S'))
    print('built detail level overlaps in ')
    print("--- %s seconds ---" % (time.time() - start_time))
    return OUTPUT_EXPOSURE_SUMMARY

print(track())




