var cacheName = 'Covidify';

var prodFilesToCache = [
    'index.html',
    'sw.js',
    'manifest.json',
    'styles.css',
    'main.js',
    'libraries/bootstrap.min.css',
    'libraries/materialize.min.css',
    'libraries/jquery.min.js',
    'libraries/bootstrap.min.js',
    'libraries/materialize.min.js',
    'libraries/select.js'
]

/* Start the service worker and cache all of the app's content yeah boy */
self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            return cache.addAll(prodFilesToCache);
        })
    );
});

/* Serve cached content when offline 
self.addEventListener('fetch', function(e) {
    e.respondWith(
        caches.match(e.request).then(function(response) {
            return response || fetch(e.request);
        })
    );
});
*/
self.addEventListener("fetch", function (event) {
    event.respondWith(
      fetch(event.request).catch(function () {
        return caches.match(event.request);
      })
    );
  });
