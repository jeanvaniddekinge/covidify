// declare variables
var appVersion = "0.18";
var currentRecordCount = localStorage.getItem("currentRecordCount");
var illString = "";
var illStringArray = [];
var covidStatus = 0;
var logLevel = 0;
var maxRecordCount = 5;
var daysToGoBack = 7; // how many days history to send to the server
var apiTrackUrl = "https://www.covidify.co.za/covidifyapi/covidify/track";
var apiDbUrl = "https://www.covidify.co.za/covidifyapi/covidify/db";
var apiRiskUrl = "https://www.covidify.co.za/covidifyapi/covidify/risk";
var logText = "loading...\r\n";
var userAge = -1;
var userSex = -1;
var userID = "-1";
var oldlong = "";
var oldlat = "";

// redirect if missing www
console.log(window.location.hostname);
if (window.location.hostname == "covidify.co.za") {
  window.location.href = "https://www.covidify.co.za";
}

// materialize delegates
$(document).ready(function () {
  $("select").formSelect();
  $(".tabs").tabs();
});

var buttonGetRisk = document.getElementById("getRisk");
buttonGetRisk.addEventListener("click", getRisk);

// fetch all settings from local storage
function refreshFromLS() {
  //get from storage, push to ui

  illString = $.trim(localStorage.getItem("userIllness"));
  illStringArray = illString.split(",");
  $("#userIllness").val(illStringArray);

  userID = localStorage.getItem("userCell");
  if (userID == null) {
    userID = "";
  }
  $("#userCell").val(userID);

  covidStatus = localStorage.getItem("covidStatus");
  if (covidStatus == null) {
    covidStatus = 0;
  }
  $("#covidStatus").val(covidStatus);
  if (covidStatus == 1) {
    $("#covidStatus").prop("checked", true);
  }

  logLevel = localStorage.getItem("logLevel");
  if (logLevel == null) {
    logLevel = 0;
  }
  $("#covidStatus").val(logLevel);
  if (logLevel == 1) {
    $("#logLevel").prop("checked", true);
  }

  userAge = localStorage.getItem("userAge");
  //if (userAge == null) {
  //  userAge = -1;
  //}
  $("#userAge").val(userAge);

  userSex = localStorage.getItem("userSex");
  if (userSex == null) {
    userSex = -1;
  }
  $("#userSex").val(userSex);

  userRegion = localStorage.getItem("userRegion");
  if (userRegion == null) {
    userRegion = "None";
  }
  $("#userRegion").val(userRegion);

  currentRecordCount = localStorage.getItem("currentRecordCount");
  if (currentRecordCount == null) {
    currentRecordCount = 0;
    localStorage.setItem("currentRecordCount", 0);
  }

  $("#appInfo").text("Version: " + appVersion + " UserID: " + hashCode(userID));
}

// storage delegates
$("#covidStatus").on("click", function () {
  if ($("#covidStatus").prop("checked") == true) {
    covidStatus = 1;
    localStorage.setItem("covidStatus", covidStatus);
  } else {
    covidStatus = 0;
    localStorage.setItem("covidStatus", covidStatus);
  }
});

$("#logLevel").on("click", function () {
  if ($("#logLevel").prop("checked") == true) {
    logLevel = 1;
    localStorage.setItem("logLevel", logLevel);
  } else {
    logLevel = 0;
    localStorage.setItem("logLevel", logLevel);
  }
});

$("#userCell").on("input", function (e) {
  localStorage.setItem("userCell", $("#userCell").val());
  userID = localStorage.getItem("userCell");
  $("#appInfo").text("Version: " + appVersion + " UserID: " + hashCode(userID));
});

$("#userAge").on("input", function (e) {
  localStorage.setItem("userAge", $("#userAge").val());
  userAge = localStorage.getItem("userAge");
});

$("#userIllness").on("change", function (e) {
  localStorage.setItem("userIllness", $("#userIllness").val());
});

$("#userSex").on("change", function (e) {
  localStorage.setItem("userSex", $("#userSex").val());
  userSex = localStorage.getItem("userSex");
});

$("#userRegion").on("change", function (e) {
  localStorage.setItem("userRegion", $("#userRegion").val());
  userRegion = localStorage.getItem("userRegion");
});

// functions
async function getRisk() {
  if (localStorage.getItem("userIllness") == null) {
    localStorage.setItem("userIllness", "");
  }
  var outputText = "";
  var trackText = "";
  $("#riskResponse").text("");
  $("#contactResponse").text("");

  /* PERFORM RISK ASSESSMENT */
  var thing =
    "DIABETES_,HYPERTENSION_,HEART_,LIVER_,KIDNEY_,LUNGS_,PROSTATE_,CANCER_,OTHER_";
  var payload = {};
  payload.age = localStorage.getItem("userAge");
  payload.CHRONICCOUNT_ = 0;
  payload.HEART_ =
    localStorage.getItem("userIllness").includes("HEART_") == true ? 1 : 0;
  payload.CANCER_ =
    localStorage.getItem("userIllness").includes("CANCER_") == true ? 1 : 0;
  payload.LIVER_ =
    localStorage.getItem("userIllness").includes("LIVER_") == true ? 1 : 0;
  payload.KIDNEY_ =
    localStorage.getItem("userIllness").includes("KIDNEY_") == true ? 1 : 0;
  payload.DIABETES_ =
    localStorage.getItem("userIllness").includes("DIABETES_") == true ? 1 : 0;
  payload.LUNGS_ =
    localStorage.getItem("userIllness").includes("LUNGS_") == true ? 1 : 0;
  payload.PROSTATE_ =
    localStorage.getItem("userIllness").includes("PROSTATE_") == true ? 1 : 0;
  payload.HYPERTENSION_ =
    localStorage.getItem("userIllness").includes("HYPERTENSION_") == true
      ? 1
      : 0;
  payload.OTHER_ =
    localStorage.getItem("userIllness").includes("OTHER_") == true ? 1 : 0;
  payload.sex_female = localStorage.getItem("userSex") == "Female" ? 1 : 0;
  payload.sex_male = localStorage.getItem("userSex") == "Male" ? 1 : 0;
  payload.sex_unknown = 0;
  payload.CHRONICCOUNT_ +=
    payload.HEART_ +
    payload.CANCER_ +
    payload.LIVER_ +
    payload.KIDNEY_ +
    payload.DIABETES_ +
    payload.LUNGS_ +
    payload.PROSTATE_ +
    payload.HYPERTENSION_ +
    payload.OTHER_;

  illPayloadIn = [];
  illPayloadIn.push(payload);
  payloadString = JSON.stringify(illPayloadIn);

  if (navigator.onLine) {
    if (logLevel == 1) 
    {
      logText = logText += illPayloadIn + "\r\n";
    $("#logText").text(logText);
    }

    $.ajax(apiRiskUrl, {
      type: "post",
      data: payloadString,
      dataType: "json",
      contentType: "application/json",
      success: function (data) {
        data.forEach(function (obj) {
          outputText +=
            "The system determined that you have a " +
            obj.RiskFactor +
            " risk factor to COVID-19.";
        });
        $("#riskResponse").text(outputText);
      },
      error: function (xhr, status, error) {
        //var err = eval("(" + xhr.responseText + ")");
        if (logLevel == 1) 
        {
        logText = logText += xhr.responseText + "\r\n";
        $("#logText").text(logText);
        }
        alert("Please enter age and sex.");
      },
    });
  } else {
    outputText += "No data returned from server ";
    $("#riskResponse").text(outputText);
  }

  /* PERFORM CONTACT TRACING */
  const payloadIn = await buildUserData(daysToGoBack);
  if (payloadIn != "[]" && navigator.onLine) {
    if (logLevel == 1) 
    {
    logText = logText += payloadIn + "\r\n";
    $("#logText").text(logText);
    }

    $.ajax(apiTrackUrl, {
      type: "post",
      data: payloadIn,
      dataType: "json",
      contentType: "application/json",
      success: function (data) {
        trackText +=
          "You have been in the same areas as the following people with Covid-19: " +
          "\n" +
          "Date                              User               Proximity" +
          "\n";
        data.forEach(function (obj) {
          trackText +=
            obj.Date +
            " " +
            obj.Time +
            "     " +
            obj.userID +
            "       " +
            (obj.overlap * 100).toFixed(2) +
            "%" +
            "\n";
        });
        $("#contactResponse").text(trackText);
      },
      error: function (xhr, status, error) {
        //var err = eval("(" + xhr.responseText + ")");
        if (logLevel == 1) 
        {
        logText = logText += xhr.responseText + "\r\n";
        $("#logText").text(logText);
        }
        alert("Please enter age and sex.");
      },
    });
  } else {
    trackText += "No tracking data found";
    $("#contactResponse").text(trackText);
  }
}

function getLocation() {
  if (navigator.geolocation) {
    //navigator.geolocation.watchPosition(showPosition,error);
    navigator.geolocation.getCurrentPosition(onGpsChangeSuccess, error, {
      maximumAge: 10000,
      enableHighAccuracy: false,
      timeout: 15000,
    });
  } else {
    console.log("Geolocation is not supported by this browser.");
  }
}

/*
function followLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(showPosition, error);
  } else {
    console.log("Geolocation is not supported by this browser.");
  }
}
*/
function error(err) {
  console.log("ERROR(" + err.code + "): " + err.message);
  if (logLevel == 1) 
  {
  logText = logText += "ERROR(" + err.code + "): " + err.message + "\r\n";
  $("#logText").text(logText);
  }
  alert("please allow gps");
}

function onGpsChangeSuccess(position) {
  //Watch was triggered...
  if (logLevel == 1) 
  {
  logText = logText += "onGpsChangeSuccess..." + "\r\n";
  $("#logText").text(logText);
  logText = logText +=
    "UserID.length: " +
    userID.length +
    ",covidStatus: " +
    covidStatus +
    ",position.coords.latitude: " +
    position.coords.latitude +
    ",position.coords.longitude: " +
    position.coords.longitude +
    ",position.timestamp: " +
    position.timestamp +
    "\r\n";
  $("#logText").text(logText);
  }
  if (
    oldlong != position.coords.longitude &&
    oldlat != position.coords.longitude
  ) {
    //Changes detected
    oldlong = position.coords.longitude;
    oldlat = position.coords.latitude;

    var currentDate = convertTimeStamp(position.timestamp).substring(0, 10);
    var currentData = localStorage.getItem(currentDate);

    currentData =
      currentData +
      hashCode(userID) +
      "," +
      covidStatus +
      "," +
      position.coords.latitude +
      "," +
      position.coords.longitude +
      "," +
      position.timestamp +
      "\r\n";

    // if user id is valid write to local storage
    if (
      userID.length >= 10 &&
      isNumber(covidStatus) &&
      isNumber(position.coords.latitude) &&
      isNumber(position.coords.longitude) &&
      isNumber(position.timestamp)
    ) {
      localStorage.setItem(currentDate, currentData);
      currentRecordCount++;
      localStorage.setItem("currentRecordCount", currentRecordCount);
      // if x records have passed for positive patient, write to db
      sendInfectedData();
      getPhoneNumber();
    } else {
      if (logLevel == 1) 
      {
      logText = logText +=
        "criteria were not sufficient to post a new location.";
      ("\r\n");
      }
    }
  } else {
    //No changes detected
  }
}

/* // still haven't decided if follow is better than polling every x seconds
function showPosition(position) {
  logText = logText += "show position..." + "\r\n";
  $("#logText").text(logText);
  logText = logText +=
    "UserID.length: " +
    userID.length +
    ",covidStatus: " +
    covidStatus +
    ",position.coords.latitude: " +
    position.coords.latitude +
    ",position.coords.longitude: " +
    position.coords.longitude +
    ",position.timestamp: " +
    position.timestamp +
    "\r\n";
  $("#logText").text(logText);
  var currentDate = convertTimeStamp(position.timestamp).substring(0, 10);
  var currentData = localStorage.getItem(currentDate);

  currentData =
    currentData +
    hashCode(userID) +
    "," +
    covidStatus +
    "," +
    position.coords.latitude +
    "," +
    position.coords.longitude +
    "," +
    position.timestamp +
    "\r\n";

  // if user id is valid write to local storage
  if (
    userID.length >= 10 &&
    isNumber(covidStatus) &&
    isNumber(position.coords.latitude) &&
    isNumber(position.coords.longitude) &&
    isNumber(position.timestamp)
  ) {
    localStorage.setItem(currentDate, currentData);
    currentRecordCount++;
    localStorage.setItem("currentRecordCount", currentRecordCount);
    // if x records have passed for positive patient, write to db
    sendInfectedData();
    getPhoneNumber();
  } else {
    logText = logText += "criteria were not sufficient to post a new location.";
    ("\r\n");
  }
}
*/

function sendInfectedData() {
  if (logLevel == 1) 
  {
  logText = logText +=
    "currentRecordCount: " +
    currentRecordCount +
    ",maxRecordCount: " +
    maxRecordCount +
    ",covidStatus: " +
    covidStatus +
    "\r\n";
  $("#logText").text(logText);
  }
  if (
    currentRecordCount >= maxRecordCount &&
    covidStatus == 1 &&
    navigator.onLine
  ) {
    // loop through all records and post to server
    var dataPayload = buildUserData(daysToGoBack);
    if (dataPayload) {
      if (logLevel == 1) 
      {
      logText = logText += dataPayload + "\r\n";
      $("#logText").text(logText);
      }
      $.ajax(apiDbUrl, {
        type: "post",
        data: dataPayload,
        contentType: "application/json",
        success: function (data) {
          clearUserData();
        },
        error: function (xhr, status, error) {
          alert(xhr.responseText);
        },
      });
    }
  }
}

function hashCode(s) {
  return s.split("").reduce(function (a, b) {
    a = (a << 5) - a + b.charCodeAt(0);
    return a & a;
  }, 0);
}

function convertTimeStamp(timeStampIn) {
  const dateObject = new Date(timeStampIn);
  return dateObject.toISOString(); //2019-12-9 10:30:15
}

function getPhoneNumber() {
  var popup = document.getElementById("myPopup");
  if ($("#userCell").val().length >= 10) {
    popup.classList.remove("show");
  } else {
    popup.classList.add("show");
  }
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && !isNaN(n - 0);
}

function clearUserData() {
  var k = 0;
  while (k < 21) {
    var d = new Date();
    d.setDate(d.getDate() - k);
    var dateString = d.toISOString().split("T")[0];
    //console.log("clearing storage: " + dateString);
    localStorage.removeItem(dateString);
    k += 1;
  }
  localStorage.setItem("currentRecordCount", 0);
  currentRecordCount = 0;
}

function cleanOldData() {
  // cheeck and remove any data exists older than daysIn
  var fromPeriods = daysToGoBack + 1;
  var d = new Date();
  d.setDate(d.getDate() - fromPeriods);
  var dateString = d.toISOString().split("T")[0];
  var currentData = localStorage.getItem(dateString);
  console.log("checking if data must be cleaned up for " + dateString)
  if (currentData) {
    localStorage.removeItem("dateString");
  }

}

function logSystem() {
  // cheeck and remove any data exists older than daysIn
  for(var k=0; k < daysToGoBack; k++)
  {
    var d = new Date();
    d.setDate(d.getDate() - k);
    var dateString = d.toISOString().split("T")[0];
    //console.log(dateString)
    var currentData = localStorage.getItem(dateString);
    
    if (currentData) {
      var hits = (currentData.match(new RegExp("\r\n", "g")) || []).length; //logs 4
      //console.log("checking number of GPS records for " + dateString + " and found " + hits + "records" )
        logText += "Date: " + dateString + " GPS Records: " + hits + "\r\n";
      $("#logText").text(logText);
    }
  }

  
}

function buildUserData(daysIn) {
  var returnJson = [];

  var k = 0;
  while (k < daysIn) {
    var d = new Date();
    d.setDate(d.getDate() - k);
    var dateString = d.toISOString().split("T")[0];
    var currentData = localStorage.getItem(dateString);
    if (currentData) {
      // the array is defined and has at least one element
      var rowsArr = currentData.split("\r\n");
      var rowsArrLength = rowsArr.length;

      for (var i = 0; i < rowsArrLength - 1; i++) {
        var rowArr = rowsArr[i].split(",");
        var tempJson = {
          userID: rowArr[0],
          covidStatus: rowArr[1],
          latitude: rowArr[2],
          longitude: rowArr[3],
          timestamp: rowArr[4],
          Date: dateString,
        };
        returnJson.push(tempJson);
        //print(tempJson)
      }
    }
    k += 1;
  }
  return JSON.stringify(returnJson);
  //return returnJson;
}

logSystem();
refreshFromLS();
//followLocation();
getLocation();

window.setInterval(function () {
  console.log("checking if data must be sent");
  cleanOldData();
  logSystem();
  getLocation();
  sendInfectedData();
}, 10000);
